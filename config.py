# -*- coding: utf-8 -*-
import os
from os import environ
from dotenv import load_dotenv

# Load Environment Variables from .env File
load_dotenv('.env')

class Config(object):
    '''
    Configurações Gerais
    '''

    SQLALCHEMY_TRACK_MODIFICATIONS = environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')
    SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI')
    SECRET_KEY = environ.get('SECRET_APP_KEY')


class DevConfig(Config):
    '''
    Configurações de Desenvolvimento
    '''
    
    SQLALCHEMY_ECHO = True
    FLASK_DEBUG = True
    FLASK_ENV = 'development'

    DB_SERVER = '127.0.0.1'
    
    # TODO --> Add URI to dev db and remove remove SECRET_KEY from father class
    


class ProdConfig(Config):
    '''
    Configurações de Produção
    '''
    
    FLASK_DEBUG = False
    SQLALCHEMY_ECHO = False
    FLASK_ENV = 'production'

    DB_SERVER = '0.0.0.0'
    
    
    
app_config = {
    'dev': DevConfig,
    'prod': ProdConfig
}
