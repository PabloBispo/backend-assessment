from os import environ

from src import create_app


# Creating flask application with config setted on .env
config_name = environ.get('FLASK_CONFIG')
app = create_app(config_name)


if __name__ == '__main__':
    app.run()
    