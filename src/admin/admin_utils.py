from ..models import GrupoUsuario
from flask_login import current_user
from flask import flash, redirect, url_for, render_template, abort

from functools import wraps

# Decorator usado para filtrar o grupo de usuário

def grupo_usuario_wrapper(grupo_usuario, is_api=False):
    ''' O decorator verifica se o usuário que está
        logado possui permissão para acessar determinada view
        de acordo com a tabela grupo_usuario do banco '''
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwds):
            _grupos_usuario = GrupoUsuario.query.filter(
                GrupoUsuario.fk_usuario == current_user.id
            ).filter(
                GrupoUsuario.nome_grupo.in_(grupo_usuario
            )).all()
            if is_api and not _grupos_usuario:
                abort(401)
            elif not _grupos_usuario:
                flash('Você não tem permissão para acessar essa página')
                return redirect(url_for('admin.painel_de_controle'))
                
            return f(*args, **kwds)
        return wrapper
    return decorator
