from flask import render_template, jsonify
from flask_login import login_required, current_user
from flask_restful import reqparse, abort, Resource

from ..models import Produto, Catalogo, Usuario, Cliente, Estoque

from . import admin

from .. import db

from .admin_utils import grupo_usuario_wrapper


@admin.route('/painel-de-controle')
@login_required
@grupo_usuario_wrapper(['analista_pedidos', 'analista_logistico'])
def painel_de_controle():
    """
    Painel de controle principal, com rotas para as demais views
    """
    return render_template('admin/painel-de-controle.html', title="Painel de Controle")


@admin.route('/painel-de-controle/produtos')
@login_required
@grupo_usuario_wrapper(['analista_pedidos', 'analista_logistico'])
def produtos():
    ''' View de listagem de produtos '''
    _produtos = ViewProdutos()
    return render_template('admin/produtos.html', produtos=_produtos.get()['produtos'])


@admin.route('/painel-de-controle/cliente')
@login_required
@grupo_usuario_wrapper(['analista_pedidos', 'analista_logistico'])
def cliente():
    ''' View de listagem de clientes '''
    _clientes = ViewClientes()
    return render_template('admin/cliente.html', clientes=_clientes.get()['clientes'])


@admin.route('/painel-de-controle/estoque')
@login_required
@grupo_usuario_wrapper(['analista_pedidos', 'analista_logistico'])
def estoque():
    ''' View de listagem de estoque '''
    _estoque = ViewEstoque()
    return render_template('admin/estoque.html', estoque=_estoque.get()['estoque'])



produto_parser = reqparse.RequestParser()
# Args to filter Produto
produto_parser.add_argument('id_produto', type=int)
produto_parser.add_argument('nome_produto', type=str)
produto_parser.add_argument('lote', type=str)
produto_parser.add_argument('prazo_validade_em_meses', type=int)
produto_parser.add_argument('id_produto_update', type=int)
produto_parser.add_argument('id_produto_delete', type=int)




class ViewProdutos(Resource):
    ''' 
    Crud de Produto que permite ao analista de pedidos realizar todas as operações,
    e o analista logistico pode apenas vizualizar os produtos
    '''
    @login_required
    @grupo_usuario_wrapper(['analista_pedidos', 'analista_logistico'], is_api=True)
    def get(self):

        args = produto_parser.parse_args()

        query_produtos = Produto.query
        print(args.values())
        if any(args.values()):
            try:
                for key, value in args.items():
                    if value:
                        query_produtos = query_produtos.filter(
                            getattr(Produto, key) == value
                        )    
            except:
                pass
        query_produtos = query_produtos.all()

        return {"produtos":[p.serialize for p in query_produtos]}


    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)
    def post(self):
        args = produto_parser.parse_args()
        
        new_produto = Produto(
            nome_produto=args.nome_produto,
            lote=args.lote,
            prazo_validade_em_meses=args.prazo_validade_em_meses
            )

        db.session.add(new_produto)
        db.session.commit()

        print(new_produto.__repr__())

        return new_produto.__repr__(), 

         
    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)    
    def put(self):
        args = produto_parser.parse_args()
        
        id_produto_update = args.id_produto_update
        produto_update = Produto.query.filter_by(id_produto=id_produto_update).first()

        if produto_update:
            if args.nome_produto:
                produto_update.nome_produto = args.nome_produto
            if produto_update.lote:
                produto_update.lote = args.lote
            if produto_update.prazo_validade_em_meses:
                produto_update.prazo_validade_em_meses = args.prazo_validade_em_meses

            db.session.commit()

            print(produto_update.__repr__())

            return produto_update.__repr__(), 201
        else:
            return "ID inválido", 404
            

    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)
    def delete(self):
        args = produto_parser.parse_args()
        id_produto_delete = args.id_produto_delete
        produto_delete = Produto.query.filter_by(id_produto=id_produto_delete).delete()
        print("###",produto_delete)
        if produto_delete:
            db.session.commit()
            return "Produto deletado com sucesso.", 200
        else:
            return "ID inválido", 404



cliente_parser = reqparse.RequestParser()
# Args to filter Produto
cliente_parser.add_argument('id_cliente', type=int)
cliente_parser.add_argument('nome', type=str)
cliente_parser.add_argument('email', type=str)
cliente_parser.add_argument('cpf', type=str)
cliente_parser.add_argument('id_cliente_update', type=str)
cliente_parser.add_argument('id_cliente_delete', type=str)



class ViewClientes(Resource):
    ''' 
    Crud de Clientes que permite ao analista de pedidos realizar todas as operações,
    e o analista logistico pode apenas vizualizar os clientes
    '''
    @login_required
    @grupo_usuario_wrapper(['analista_pedidos', 'analista_logistico'], is_api=True)
    def get(self):

        args = cliente_parser.parse_args()

        query_clientes = Cliente.query
        print(args.values())
        if any(args.values()):
            try:
                for key, value in args.items():
                    if value:
                        query_clientes = query_clientes.filter(
                            getattr(Cliente, key) == value
                        )
            except:
                pass
        query_clientes = query_clientes.all()

        return {"clientes":[c.serialize for c in query_clientes]}


    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)
    def post(self):
        args = cliente_parser.parse_args()

        new_cliente = Cliente(
            nome=args.nome,
            email=args.email,
            cpf=args.cpf
            )

        db.session.add(new_cliente)
        db.session.commit()

        print(new_cliente.__repr__())

        return new_cliente.__repr__(), 
    

    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)
    def put(self):
        args = cliente_parser.parse_args()
        
        id_cliente_update = args.id_cliente_update
        cliente_update = Cliente.query.filter_by(id_cliente=id_cliente_update).first()

        if cliente_update:
            if args.nome:
                cliente_update.nome = args.nome
            if cliente_update.email:
                cliente_update.email = args.email
            if cliente_update.cpf:
                cliente_update.cpf = args.cpf

            db.session.commit()

            print(cliente_update.__repr__())

            return cliente_update.__repr__(), 201
        else:
            return "ID inválido", 404


    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)
    def delete(self):
        args = cliente_parser.parse_args()
        id_cliente_delete = args.id_cliente_delete
        cliente_delete = Cliente.query.filter_by(id_cliente=id_cliente_delete).delete()
        if cliente_delete:
            db.session.commit()
            return "Cliente deletado com sucesso.", 200
        else:
            return "ID inválido", 404



estoque_parser = reqparse.RequestParser()
# Args to filter Produto
estoque_parser.add_argument('id_estoque', type=int)
estoque_parser.add_argument('quantidade', type=int)
estoque_parser.add_argument('localizacao', type=str)
estoque_parser.add_argument('fk_produto', type=int)
estoque_parser.add_argument('fk_cliente', type=int)
estoque_parser.add_argument('id_estoque_update', type=int)
estoque_parser.add_argument('id_estoque_delete', type=int)



class ViewEstoque(Resource):
    ''' 
    Crud de Estoque que permite ao analista de pedidos realizar todas as operações,
    e o analista logistico pode apenas vizualizar o estoque
    '''
    @login_required
    @grupo_usuario_wrapper(['analista_pedidos', 'analista_logistico'], is_api=True)
    def get(self):

        args = estoque_parser.parse_args()

        query_estoque = Estoque.query
        print(args.values())
        if any(args.values()):
            for key, value in args.items():
                try:
                    if value:
                        query_estoque = query_estoque.filter(
                            getattr(Estoque, key) == value
                        )    
                except:
                    pass
        query_estoque = query_estoque.all()

        return {"estoque":[e.serialize for e in query_estoque]}


    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)
    def post(self):
        args = estoque_parser.parse_args()
        try:
            produto = Produto.query.filter_by(
                    id_produto=args.fk_produto).first()
            if not produto:
                return "ID produto Inválido", 400

            cliente = Cliente.query.filter_by(
                    id_cliente=args.fk_cliente).first()
            if not cliente:
                return "ID cliente Inválido", 400
                
            new_estoque = Estoque(
                quantidade = args.quantidade,
                fk_produto = produto.id_produto,
                fk_cliente = cliente.id_cliente,
                localizacao = args.localizacao
                )

            db.session.add(new_estoque)
            db.session.commit()

            print(new_estoque.__repr__())

            return new_estoque.__repr__(), 
        except:
            return "Bad Request, verify request args", 400
        

    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)
    def put(self):
        args = estoque_parser.parse_args()
        
        id_estoque_update = args.id_estoque_update
        estoque_update = Estoque.query.filter_by(id_estoque=id_estoque_update).first()
        try:
            if estoque_update:
                if args.quantidade:
                    estoque_update.quantidade = args.quantidade
                if estoque_update.fk_produto:
                    produto = Produto.query.filter_by(
                        id_produto=args.fk_produto).first()
                    if produto:
                        estoque_update.fk_produto = produto.id_produto
                    else:
                        return "ID Produto inválido", 400
                if estoque_update.fk_cliente:
                    cliente = Cliente.query.filter_by(
                        id_cliente=args.fk_cliente).first()
                    if cliente:
                        estoque_update.fk_cliente = cliente.id_cliente
                    else:
                        return "ID Cliente inválido", 400
                if estoque_update.localizacao:
                    estoque_update.localizacao = args.localizacao
            else:
                return "ID inválido", 404
        except Exception as e:
            print(e)
            return "Bad Request, verify request args", 400

        db.session.commit()

        print(estoque_update.__repr__())

        return estoque_update.__repr__(), 201
        
    

    @login_required
    @grupo_usuario_wrapper(['analista_pedidos'], is_api=True)
    def delete(self):
        args = estoque_parser.parse_args()
        id_estoque_delete = args.id_estoque_delete
        estoque_delete = Estoque.query.filter_by(id_estoque=id_estoque_delete).delete()
        if estoque_delete:
            db.session.commit()
            return "Item do estoque deletado com sucesso.", 200
        else:
            return "ID inválido", 404
