from flask import Flask
from flask_sqlalchemy import SQLAlchemy


from config import app_config
from flask_login import LoginManager

from flask_bootstrap import Bootstrap

from flask_restful import Api


# Variável DB do Flask-SQLAlchemy
db = SQLAlchemy()

# Login Manager do Flask-Login
login_manager = LoginManager()


def create_app(config_name):
    '''Function that creates the app
    with respective configurations,
    settig blueprints, adding endpoints, etc'''
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])

    api = Api(app)

    db.init_app(app)

    login_manager.init_app(app)
    login_manager.login_message = "Você precisa estar logado para acessar essa página."
    login_manager.login_view = "auth.login"

    # Used by flask-form
    Bootstrap(app)

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)


    from .admin import ViewProdutos, ViewClientes, ViewEstoque
    
    # Restful endpoins to 'Produtos', 'Clientes' and 'Estoque'
    api.add_resource(ViewProdutos, '/api/produtos')
    api.add_resource(ViewClientes, '/api/clientes')
    api.add_resource(ViewEstoque, '/api/estoque')

    return app