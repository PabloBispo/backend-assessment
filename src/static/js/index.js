function remove_produto(produto_id){
  axios({
      method: 'delete',
      url: '/api/produtos',
      data: {
        id_produto_delete: produto_id
      }
    }).then((response) => {
      console.log('removido com sucesso!')
      document.getElementById('produto_' + produto_id).style.display = "none";
    });
}

function remove_cliente(cliente_id){
  axios({
      method: 'delete',
      url: '/api/clientes',
      data: {
        id_cliente_delete: cliente_id
      }
    }).then((response) => {
      console.log('removido com sucesso!')
      document.getElementById('cliente_' + cliente_id).style.display = "none";
    });
}

function remove_estoque(estoque_id){
  axios({
      method: 'delete',
      url: '/api/estoque',
      data: {
        id_estoque_delete: estoque_id
      }
    }).then((response) => {
      console.log('removido com sucesso!')
      document.getElementById('estoque_' + estoque_id).style.display = "none";
    });
}

document.addEventListener("DOMContentLoaded", function(){
  document.getElementById("painel-container").focus();
});
