from flask import flash, redirect, render_template, url_for, request
from flask_login import login_required, login_user, logout_user, current_user

from . import auth
from .forms import LoginForm, RegistrationForm
from .. import db
from ..models import Usuario


@auth.route('/register', methods=['GET', 'POST'])
def register():
    """
    Endpoint to Registration Form
    """

    if current_user.is_authenticated:
        return redirect(url_for('admin.painel_de_controle'))

    form = RegistrationForm()
    if form.validate_on_submit():
        usuario = Usuario(email=form.email.data,
                            nome=form.nome.data,
                            password=form.senha.data)

        db.session.add(usuario)
        db.session.commit()
        flash('Usuário cadastrado com sucesso.')

        return redirect(url_for('auth.login'))

    return render_template('auth/register.html', form=form, title='Register')


@auth.route('/login', methods=['GET', 'POST'])
def login():
    print("method",request.method)
    """
   Endpoint to Login form
    """

    if current_user.is_authenticated:
        return redirect(url_for('admin.painel_de_controle'))

    form = LoginForm()
    
    if form.validate_on_submit() or 1:
        print('here')

        # verify user credentials (email, senha)
        usuario = Usuario.query.filter_by(email=form.email.data).first()
        if usuario is not None and usuario.verificar_senha(
                form.senha.data):
            
            #sign in user
            login_user(usuario)
            
            return redirect(url_for('admin.painel_de_controle'))
        
        else:
            flash('Email ou senha inválidos.')

    return render_template('auth/login.html', form=form, title='Login')


@auth.route('/logout')
@login_required
def logout():
    """
    Endpoint o Logout Form
    """
    logout_user()
    flash('Até a próxima! =)')

    # redirect to the login page
    return redirect(url_for('home.homepage'))