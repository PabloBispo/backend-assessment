from flask_login import UserMixin
#from .utils import gerar_hash_senha, verificar_senha_hash

from werkzeug.security import generate_password_hash, check_password_hash
from src import db, login_manager
from time import time


class Catalogo(db.Model):
    ''' Catálogo de produtos, contendo referências(db) ao catálogo e produto,
    nome do catálogo, preço do produto e referência ao "dono" do catálogo (cliente)'''

    __tablename__ = 'catalogo'

    id_catalogo = db.Column(db.Integer, primary_key=True)
    fk_produto = db.Column(db.Integer, db.ForeignKey('Produto.id_produto'))
    nome_catalogo = db.Column(db.String)
    preco_produto_reais = db.Column(db.Float)
    fk_cliente = db.Column(db.Integer, db.ForeignKey('Produto.id_cliente'))


class Cliente(db.Model):
    ''' <Clientes> Contendo nome, email e CPF do cliente.
    Propriedade "serialize" --> dict '''

    __tablename__ = 'cliente'

    id_cliente = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String)
    email = db.Column(db.String)
    cpf = db.Column(db.String)


    @property
    def serialize(self):
        return {
            'id_cliente': self.id_cliente,
            'nome': self.nome,
            'email': self.email,
            'cpf': self.cpf
       }

    def __repr__(self):
        return {"Cliente": self.serialize}


class Estoque(db.Model):
    ''' <Estoque> Contendo quantidade, localização e referências aos produtos contidos no estoque
    Propriedade "serialize" --> dict '''

    __tablename__ = 'estoque'

    id_estoque = db.Column(db.Integer, primary_key=True)
    quantidade = db.Column(db.Integer)
    fk_produto = db.Column(db.Integer, db.ForeignKey('produto.id_produto'))
    fk_cliente = db.Column(db.Integer, db.ForeignKey('cliente.id_cliente'))
    localizacao = db.Column(db.String)
    

    @property
    def serialize(self):
        return {
            'id_estoque': self.id_estoque,
            'quantidade': self.quantidade,
            'fk_produto': self.fk_produto,
            'localizacao': self.localizacao,
            'fk_cliente': self.fk_cliente
       }

    def __repr__(self):
        return {"Estoque": self.serialize}
        

class GrupoUsuario(db.Model):
    ''' <GrupoUsuario> Contendo o nome do grupo, referẽncias à permissão do grupo e respectivos usuários'''
    __tablename__ = 'grupo_usuario'

    id_grupo_usuario = db.Column(db.Integer, primary_key=True)
    nome_grupo = db.Column(db.String)
    fk_permissao = db.Column(db.Integer, db.ForeignKey('Permissao.id_permissao'))
    fk_usuario = db.Column(db.Integer, db.ForeignKey('Usuario.id'))


class Pedido(db.Model):
    ''' <Pedido> Contendo referência aos produtos no pedido,
     quantidade de cada produto, data do pedido,
     data de entrega e local de entrega '''
    __tablename__ = 'pedidos'

    id_pedido = db.Column(db.Integer, primary_key=True)
    fk_produto = db.Column(db.Integer, db.ForeignKey('Produto.id_produto'))
    quantidade = db.Column(db.Integer)
    data_pedido = db.Column(db.DATETIME)
    data_entrega = db.Column(db.DATETIME)
    local_entrega = db.Column(db.String)


class Permissao(db.Model):
    __tablename__ = 'permissao'

    id_permissao = db.Column(db.Integer, primary_key=True)
    create = db.Column(db.Boolean, default=False, nullable=False)
    read = db.Column(db.Boolean, default=True, nullable=False)
    update = db.Column(db.Boolean, default=False, nullable=False)
    delete = db.Column(db.Boolean, default=False, nullable=False)
    view_endpoint = db.Column(db.String, nullable=False)
    permission_name = db.Column(db.String)


class Produto(db.Model):
    ''' <Estoque> Contendo nome do produto, lote e prazo de validade
    Propriedade "serialize" --> dict '''

    __tablename__ = 'produto'

    nome_produto = db.Column(db.String)
    id_produto = db.Column(db.Integer, primary_key=True)
    #data_fabricacao = db.Column(db.DATETIME)
    lote = db.Column(db.String)
    prazo_validade_em_meses = db.Column(db.Integer)

    @property
    def serialize(self):
        return {
            'id_produto': self.id_produto,
            'nome_produto': self.nome_produto,
            'lote': self.lote,
            'prazo_validade_em_meses': self.prazo_validade_em_meses
       }

    def __repr__(self):
        return {"Produto": self.serialize}

class Usuario(UserMixin, db.Model):
    ''' <Usuario> Contendo email, nome, hash da senha e grupo de usuario
    Propriedade "password" --> Impede que password() seja acessado '''

    __tablename__ = 'usuario'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String)
    nome = db.Column(db.String)
    hash_senha = db.Column(db.String)
    grupo_usuarios = db.Column(db.String)


    @property
    def password(self):
        raise AttributeError('password is not a readable attribute.')


    @password.setter
    def password(self, password):
        self.hash_senha = generate_password_hash(password)


    def verificar_senha(self, password):
        """
        Check if hashed password matches actual password
        """
        print(f'### hash_senha-->{self.hash_senha}###')
        print(f'### password-->{generate_password_hash(password)}###')
        return check_password_hash(self.hash_senha, password)

    
    def __repr__(self):
        return f'<Usuário: {self.nome}>'


# Carregamento do usuário feito pelo Login manager do flask
@login_manager.user_loader
def load_user(user_id):
    return Usuario.query.get(int(user_id))
