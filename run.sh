echo Install dependencies

python3 -m venv venv
source venv/bin/activate

pip3 install --upgrade setuptools
pip3 install --no-cache -r requirements.txt

flask run

